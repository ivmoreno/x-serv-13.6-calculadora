#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

"""
Simple Calculator
"""

def suma(numb1, numb2):

    print("your sum is: ", float(numb1)+float(numb2))


def resta(numb1, numb2):
    print("your sub is: ", float(numb1)-float(numb2))


try:

    if sys.argv[1] == "suma":
        suma(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == "resta":
        resta(sys.argv[2], sys.argv[3])

except ValueError:
    print("could not convert string to float: ", sys.argv[1], sys.argv[2], sys.argv[3])